export const mediaQuery = {
    data() {
        return {
            desktop: 1024,
            tablet: 768,
            mobile: 480,
            onMobile: false,
            onTabelt: false,
            onDesktop: false,
        }
    },
    methods: {
        setMediaQuery() {
            let windowWidth = window.innerWidth;
            if(windowWidth > this.tablet) {
                this.onDesktop = true;
                this.onTabelt = false;
                this.onMobile = false;
            }
            else if (windowWidth <= this.mobile){
                this.onDesktop = false;
                this.onTabelt = false;
                this.onMobile = true;
            }
            else {
                this.onDesktop = false;
                this.onTabelt = true;
                this.onMobile = true;
            }
        }
    },
    mounted() {
        this.setMediaQuery();
    },
    created() {
        window.addEventListener('resize', this.setMediaQuery);
    },
    destroyed() {
        window.removeEventListener('resize', this.setMediaQuery);
    },
};