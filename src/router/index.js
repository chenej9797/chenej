import Vue from 'vue'
import Router from 'vue-router'
import PageHome from '@/components/PageHome'
import PageAboutMe from '@/components/PageAboutMe'
import PageWorks from '@/components/PageWorks'
import PageContact from '@/components/PageContact'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'PageHome',
      component: PageHome
    },
    {
      path: '/about-me',
      name: 'PageAboutMe',
      component: PageAboutMe
    },
    {
      path: '/works',
      name: 'PageWorks',
      component: PageWorks
    },
    {
      path: '/contact',
      name: 'PageContact',
      component: PageContact
    },
  ]
})
